const 	messagesArea 	= document.querySelector('.messagesArea-messages'),
		buttonSend 		= document.querySelector('.button-send'),
		username 		= document.querySelector('.username'),
		text 			= document.querySelector('.text');
let intervalUpdate 		= setInterval(requestREST, 500),
	requestExists		= false,
	optionExists 		= false,
	btnExists 			= false;


requestREST(); 													// Render of existed messages


// Buttons listener
document.addEventListener('mousedown', (e) => {

	const target = e.target.closest('[method]');

	if (!(target)) return; 										// If click's target is not a request button - returns

	if (target.classList.contains('button-edit')) { 			// If target is PUT button in options - do handler
		handlePUT(target);
		return;
	}

	requestREST(target.getAttribute('method'), getId(target)); 	// Handle request
});


// Options listener
document.addEventListener('mousedown', (e) => {
	const target = e.target;

	if (!(target.classList.contains('message-body-options'))) { // If click's target is not a option - then...
		if (optionExists) { 									// ...removes all option menus resets update interval and returns
			removeOptions();
			intervalUpdate = setInterval(requestREST, 500);
		}
		return;
	}

	if (optionExists) removeOptions(); 							// If another options menu is requested - clear other

	renderOptions(target); 										// Renders options menu

	clearInterval(intervalUpdate); 								// Stops message updating process
});


// Options listener
messagesArea.addEventListener('scroll', handleScrollButton);


// Error listener
window.addEventListener('error', handleError);


// Generating object with needed REST-method
function generateMethods(request, id) {

	switch (request) {

		case 'GET':
			return {
				method	: 'GET',
				url		: `/api/chat/message`,
				body	: null
			};

		case 'POST':
			return {
				method	: 'POST',
				url		: `/api/chat/message`,
				body	: JSON.stringify({
							"username": username.value,
							"text": text.value
						})
			};

		case 'PUT':
			return {
				method	: 'PUT',
				url		: `/api/chat/message/${id}`,
				body	: JSON.stringify({
							"username": username.value,
							"text": text.value
						})
			};

		case 'DELETE':
			return {
				method	: 'DELETE',
				url		: `/api/chat/message/${id}`,
				body	: null
			};
	}
}


// Processing REST-request
function requestREST(request = 'GET', id = null) {
	const 	methods = generateMethods(request, id),
			xhr 	= new XMLHttpRequest();

	if ( isEmpty(username, text) && (request == 'POST' || request == 'PUT') ) return;

	xhr.open(methods.method, methods.url, true); 				// Compile request
	xhr.setRequestHeader('Content-Type', 'application/json');

	xhr.onload = () => { 										// When response is loaded...
		if (xhr.status != 200) { 								// ...if response status is not a 200 - throw an error notifification
			clearInterval(intervalUpdate);
			intervalUpdate = setInterval(requestREST, 6000);

			createNotification(`Status: ${xhr.status} - Connection failed! Reconnecting...`, 'error');
		
			requestExists = false;
			return;
		}

		switch (request) { 										// ...else - handle the response
			case 'GET':
				if (!requestExists) {
					clearInterval(intervalUpdate);
					intervalUpdate = setInterval(requestREST, 500);
					createNotification('Сonnection established!', 'success');
					requestExists = true;
				}
				clearField();
				renderMessages(xhr.responseText);
				break;

			case 'POST':
				clearField();
				text.value = '';
				renderMessages(xhr.responseText);
				break;

			case 'PUT':
				text.value = '';
				removeBackground('.messagesArea-footer');
				buttonSend.setAttribute('method', 'POST');
				createNotification(`Message edited!`, 'success');
				break;

			case 'DELETE':
				createNotification(`Message deleted!`, 'success');
				break;
		}
	}

	xhr.send(methods.body);
}


// PUT-request-process handler
function handlePUT(target) {
	username.value 	= getTextContent(target, '.message-sender');
	text.value 		= getTextContent(target, '.message-body-text');
	buttonSend.setAttribute('method', 'PUT');
	buttonSend.setAttribute('messageId', getId(target));
	addBackground('.messagesArea-footer');
}


// Filling messages field with messages from server
function renderMessages(dataJSON) {
	const messages 	= JSON.parse(dataJSON);
	optionExists 	= false;

	messages.forEach(element => {
		messagesArea.appendChild(createMessage(element));
	});
}


// Create message DOM element 
function createMessage(messageObj) {
	const 	mesElem 	= document.createElement('li'),
			mesSender 	= document.createElement('div'),
			mesBody 	= document.createElement('div'),
			mesText 	= document.createElement('span'),
			mesOpt 		= document.createElement('span');

	mesSender.className 	= 'message-sender';
	mesSender.textContent 	= messageObj.username;

	mesText.className 		= 'message-body-text';
	mesText.textContent 	= messageObj.text;

	mesOpt.className 		= 'message-body-options';
	mesOpt.textContent 		= '⋮';

	mesBody.className 		= 'message-body';
	mesBody.appendChild(mesText);
	mesBody.appendChild(mesOpt);

	mesElem.className 		= 'message';
	mesElem.setAttribute('messageId', messageObj.id);
	mesElem.appendChild(mesSender);
	mesElem.appendChild(mesBody);

	return mesElem;
}


// Filling messages field with messages from server
function renderOptions(target) {
	target.appendChild(createOptions(getId(target)));

	optionExists = true;
}


// Delete OPTIONS from DOM
function removeOptions() {
	const optionsElem 	= document.querySelector('.options-list');
	optionExists 		= false;

	optionsElem.remove();
}


// Create message option DOM element 
function createOptions(id) {
	const 	optionsElem = document.createElement('div'),
			buttonEdit 	= document.createElement('input'),
			buttonDel 	= document.createElement('input');

	buttonEdit.classList.add('button-edit');
	buttonEdit.setAttribute('type', 'button');
	buttonEdit.setAttribute('method', 'PUT');
	buttonEdit.setAttribute('messageId', id);
	buttonEdit.setAttribute('value', 'Edit');

	buttonDel.classList.add('button-delete');
	buttonDel.setAttribute('type', 'button');
	buttonDel.setAttribute('method', 'DELETE');
	buttonDel.setAttribute('messageId', id);
	buttonDel.setAttribute('value', 'Delete');

	optionsElem.classList.add('options-list');
	optionsElem.appendChild(buttonEdit);
	optionsElem.appendChild(buttonDel);

	return optionsElem;
}


// Returns Message ID
function getId(target) {
	return target.closest('[messageId]') ? target.closest('[messageId]').getAttribute('messageId') : null;
}


// Returns Message Sender or Text by selector
function getTextContent(target, selector) {
	return target.closest('li') ? target.closest('li').querySelector(selector).textContent : null;
}


// Check if input fields are empty
function isEmpty(...inputs) {
	let statusEmpty = false;
	
	inputs.forEach(element => {
		if (element.value.length == 0) {
			statusEmpty = true;
		}
	});

	return statusEmpty;
}


// Add input colors
function addBackground(...selectors) {
	selectors.forEach(element => {
		document.querySelector(element).classList.add('black-background');
	});
}


// Remove input colors
function removeBackground(...selectors) {
	selectors.forEach(element => {
		document.querySelector(element).classList.remove('black-background');
	});
}


// Clears messages field
function clearField() {
	messagesArea.textContent = '';
}


// Scrolls messages field to the bottom
function scrollBottom() {
	messagesArea.scrollTop = messagesArea.scrollHeight;
}


// Scroll bottom button if needed
function handleScrollButton() {
	const 	scrollPosition 	= messagesArea.scrollHeight - messagesArea.scrollTop - messagesArea.offsetHeight,
			footer 			= document.querySelector('.messagesArea-footer');

	if (!btnExists && scrollPosition > messagesArea.offsetHeight) { // If Scroll is too high...
		const scrollButton = createScrollButton(); 					// ...creates scroll down button
		footer.appendChild(scrollButton);
		btnExists = true;
		return;
	}

	if (btnExists && scrollPosition <= messagesArea.offsetHeight) { // If scroll is low - remove scroll button
		footer.lastChild.style.transform 	= 'translate(0, 10px)'; // Animation of appearance
		footer.lastChild.style.opacity 		= '0';

		setTimeout(() => {
			footer.removeChild(footer.lastChild);
		}, 500);

		btnExists = false;
	}
}


// Scroll bottom button if needed
function createScrollButton() {
	const scrollButton = document.createElement('div');

	scrollButton.classList.add('scroll-bottom');
	scrollButton.textContent 	= '⮟';
	scrollButton.style.opacity 	= '0';

	setTimeout(() => { 												// Animation of appearance 
		scrollButton.style.transform 	= 'translate(0, 0)';
		scrollButton.style.opacity 		= '1';
	}, 100);

	scrollButton.addEventListener('mousedown', scrollBottom);

	return scrollButton;
}


// Error event handler
function handleError(e) {
	const text = `${e.message} is occured!`

	createNotification(text, 'error');
}


// Error or REST Pop-up creation
function createNotification(message, type) {
	const 	popupBox 	= document.querySelector('.notification-box'),
			popup 		= document.createElement('div');

	popup.className 	= `notification ${type}`;
	popup.textContent 	= message;

	popupBox.appendChild(popup);

	setTimeout(() => { 												// Notification 'IN' animation
		popup.style.transform = 'translate(0, 100%)';
	}, 500);

	setTimeout(() => { 												// Notification 'OUT' animation
		popup.style.transform = 'translate(0, 0)';
		setTimeout(() => {
			popupBox.removeChild(popup);
		}, 600);
	}, 5000);
}